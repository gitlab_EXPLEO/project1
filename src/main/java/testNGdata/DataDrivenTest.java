package testNGdata;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import util.TestUtil;




public class DataDrivenTest {
	
	WebDriver driver;
	
	@BeforeMethod
	public void setup()
	{
		System.setProperty("webdriver.chrome.driver","F:\\Education\\Testing\\chromedriver_win32\\chromedriver.exe");
		
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
		
		driver.manage().window().maximize();
	    driver.manage().deleteAllCookies();
	    driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  
	     
	}
	

	

	@DataProvider
	public Object getLoginData()
	{
		
		Object data[][]=TestUtil.getTestData("RegTestData");
		return data;
	}
	
	
	
	@Test(dataProvider = "getLoginData")
	public void loginTest(String fname,String lname,String mobile,String pass) throws InterruptedException
	{
		
		Thread.sleep(2000);
	
		driver.findElement(By.xpath("//a[@role='button'][contains(text(),'Create New Account')]")).click();
		 driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys(fname);
         driver.findElement(By.xpath("//input[@name='lastname']")).sendKeys(lname);
	     driver.findElement(By.xpath("//input[@name='reg_email__']")).sendKeys(mobile);
	     driver.findElement(By.xpath("//input[@name='reg_passwd__']")).sendKeys(pass);
	     driver.findElement(By.xpath("//div[@class='_1lch']//button[contains(text(),'Sign Up')]")).click();
	   
	     
	}
	
//   @AfterMethod
//   public void tearDown()
//   {
//	   driver.quit();
//   }
}
